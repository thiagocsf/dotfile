export PATH=~/bin:/usr/local/bin:/usr/local/opt/openjdk/bin:$PATH

# https://github.com/Backblaze/B2_Command_Line_Tool/issues/500#issuecomment-415654382
# export BASH_COMPLETION_COMPAT_DIR="/usr/local/etc/bash_completion.d"
[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"

# Git prompt
# https://github.com/magicmonty/bash-git-prompt#installation
if [ -f "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh" ]; then
  __GIT_PROMPT_DIR=$(brew --prefix)/opt/bash-git-prompt/share
  source "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh"
fi
if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
  GIT_PROMPT_ONLY_IN_REPO=1
  source $HOME/.bash-git-prompt/gitprompt.sh
fi

# Misc MacOS aliases
alias fucking_osx_dns='sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder'
alias listening='lsof -iTCP -sTCP:LISTEN'
alias top='top -u -s5'

# With colours
alias ll='ls -Glart'
alias less='less -R'
export LSCOLORS=ExFxCxDxBxegedabagacad

# Bash customisation
export HISTCONTROL="erasedups:ignorespace"

# Git aliases
alias g='git'
alias ga='git add'
alias gc='git checkout'
alias gcam='git commit -am'
alias gcd='git checkout develop'
alias gcm='git checkout master'
alias gcom='git commit -m'
alias gd='git diff'
alias gdc='git diff --cached'
alias gfa='git fetch --all'
alias gfo='git fetch origin'
alias gl='git log --show-signature'
alias gp='git pull'
alias gpl="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
alias gr='git rebase'
alias grod='git rebase origin/develop'
alias grom='git rebase origin/master'
alias gs='git status'
# FIXME
#alias gsu="git branch --set-upstream-to origin/$(git symbolic-ref HEAD | sed -e 's#^refs/heads/##')"

# Python alises
alias 'pip-compile'='pip-compile --no-index --no-emit-trusted-host'

# Local settings
[[ -r ~/.profile ]] && source ~/.profile

