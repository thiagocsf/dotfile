# Git aliases
alias g='git'
alias ga='git add'
alias gc='git checkout'
alias gcam='git commit -am'
alias gcd='git checkout develop'
alias gcm='git checkout master'
alias gcom='git commit -m'
alias gd='git diff'
alias gdc='git diff --cached'
alias gfa='git fetch --all'
alias gfo='git fetch origin'
alias gl='git log --show-signature'
alias gp='git pull'
alias gpl="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
alias gr='git rebase'
alias grod='git rebase origin/develop'
alias grom='git rebase origin/master'
alias gs='git status'

if [ -f "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh" ]; then
  __GIT_PROMPT_DIR=$(brew --prefix)/opt/bash-git-prompt/share
  GIT_PROMPT_ONLY_IN_REPO=1
  source "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh"
fi
