set nocompatible              " be iMproved, required
filetype off  " for Vundle

" start Vundle
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Vimjas/vim-python-pep8-indent'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" end Vundle

syntax on
let mapleader = ","
set nocompatible
set encoding=utf-8

" appearance
set number
"set visualbell
" Last line
set ruler
set laststatus=2
set showmode
set showcmd

set listchars=tab:▸\ ,eol:¬
map <leader>l :set list!<CR> " Toggle tabs and EOL

" Searching
nnoremap / /\v
vnoremap / /\v
set hlsearch
set incsearch
set ignorecase
set smartcase
set showmatch
map <leader><space> :let @/=''<cr> " clear search

" indentation/tabulation
set wrap
set textwidth=99
set formatoptions=tcqrn1
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set noshiftround
map <leader>q gqip

au Filetype python set
    \  tabstop=4
    \  softtabstop=4
    \  shiftwidth=4
    \  autoindent
    \  fileformat=unix

" Cursor motion
set scrolloff=3
set backspace=indent,eol,start
set matchpairs+=<:> " use % to jump between pairs
runtime! macros/matchit.vim
set ttyfast


" disable mode lines
set modelines=0

" Color scheme (terminal)
set t_Co=256
set background=dark
let g:solarized_termcolors=256
let g:solarized_termtrans=1
" curl -o ~/.vim/colors/ https://raw.github.com/altercation/vim-colors-solarized/master/colors/solarized.vim
colorscheme solarized

" languagetool
Plugin 'dpelle/vim-LanguageTool'
let g:languagetool_jar='$HOME/LanguageTool-5.0-SNAPSHOT/languagetool-commandline.jar'
